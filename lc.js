const http = require('https');
const request = require("request");
const fs = require("fs");
const download = require('download-file')
let board = process.argv[2];
const threadNumber = process.argv[3];
if (board == "Ω") {
    board = "%CE%A9";
} else if (board == "Δ") {
    board = "Δ";
} else if (board == "λ") {
    board = "%CE%BB";
}

const fullUrl = `https://www.lainchan.org/${board}/res/${threadNumber}.json`;
request(fullUrl, function (error, response, body) {
    var results = JSON.parse(body);
    let count = 0;
    let files = [];
    for (let i = 0; i < results.posts.length; i++) {

        if (results.posts[i].filename == null) {

        } else if (results.posts[i].ext == "deleted") {
            console.log("This file has been deleted");
        } else {
            ++count;
            files[count] = `https://www.lainchan.org/${board}/src/${results.posts[i].tim}${results.posts[i].ext}`;
        }

    }
    var dir = threadNumber;
    if (!fs.existsSync("./dowloads/" + dir)) {
        fs.mkdirSync("./downloads/" + dir);
        for (let i = 0; i < files.length; i++) {
            if (files[i] == null) {
                console.log("file is invalid or corrupt.");
            } else {

                var filename = files[i].split("/");
                filename = filename[5];
                var options = {
                    directory: "./downloads/" + dir + "/",
                    filename: filename
                }
                download(files[i], options, function (err) {
                    if (err) throw err
                    console.log(`Downloading ${files[i]}`);
                    console.log("Success!");
                })

            }
        }
    } else {
        console.log("That directory already exists which means you already parsed that specific thread.");
    }



});